const express = require('express');
const app = express();
const path = require('path');
//app.use('/static', express.static('public'))
app.use(express.static('public'))
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, 'chart.html'));
});

app.get('/intern', (req,res) => {
    res.sendFile(path.join(__dirname,'intern.json'))
});


app.listen(process.env.PORT || 4000, function(){
    console.log('Your node js server is running');
});
